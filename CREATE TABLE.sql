CREATE TABLE
    `users` (
        `id` INT(11) AUTO_INCREMENT  PRIMARY KEY,
        `login` VARCHAR(30),
        `password` VARCHAR(30),
        `sex` VARCHAR(1),
        `location` VARCHAR(30)
    );