﻿<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Список участников</title>
  </head>
  <style>
    body {
      padding: 0;
      margin: 0;
      background: #f1f1f1;
      font-family: Calibri;
      padding-left: 20px;
    }
    h2 {
      font-size: 32px;
    }
    table { 
      width: 200px;
      text-align: left;
    }
  </style>
  <body>
    <h2>Список участников</h2>
    <br>
    <?php
      $connection = mysql_connect("localhost","root","");
      $db = mysql_select_db("usersdb");
      mysql_set_charset("utf-8",$connection);

      if(!$connection || !$db) {
        exit(mysql_error());
      }
      ?>
      <table cellpadding="0" cellspacing="0">

        <thead>
          <tr>
            <th width="33%"><p> </p></th>
            <th width="33%"><p>Логин</p></th>
            <th width="33%"><p>Пол</p></th>
          </tr>
        </thead>

        <tbody>
          <?php
          $result = mysql_query("SELECT login,sex,location
                      FROM users");
          while($row = mysql_fetch_array($result)) {
            ?>
            <tr>
              <td width="33%"><img width="50px" src="<?php echo $row['location']?>" alt=""></td>
              <td width="33%"><?php echo $row['login'] ?></td>
              <td width="33%" style="padding-left: 10px;;"><?php echo $row['sex'] ?></td>
            </tr>
          <?php } ?>
        </tbody>

      </table>
 
  </body>
</html>